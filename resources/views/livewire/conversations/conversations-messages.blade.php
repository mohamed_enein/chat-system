<div>

    @foreach ($messages as $message)
        @if ($message->isOwn())
            <livewire:conversations.conversations-message-own :message="$message" :key="$message->id" />
        @else
            <livewire:conversations.conversations-message :message="$message" :key="$message->id" />

        @endif

    @endforeach
</div>
