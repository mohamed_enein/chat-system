<?php

namespace App\Http\Controllers\Conversations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Conversation;
use Illuminate\Support\Facades\Auth;

class ConversationsController extends Controller
{
    public function index(Request $request)
    {
        $conversations = Auth::user()->conversations;
        return view('conversations.index', compact('conversations'));
    }

    public function show(Conversation $conversation, Request $request)
    {
        $conversations = Auth::user()->conversations;

        $request->user()->conversations()->updateExistingPivot($conversation, [
            'read_at' => now()
        ]);
        return view('conversations.show', compact('conversations', 'conversation'));
    }
}
